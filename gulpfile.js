var gulp        = require('gulp');
var browserSync = require('browser-sync').create();

gulp.task('default', function() {
    var files = [
    		'./**/*',
    	];
    browserSync.init({
    	files : files,
    	watchOptions: {
    	    ignoreInitial: true,
    	    ignored: 'node_modules/*'
    	},
        proxy: 'localhost:1115',
        port: '3115',
        online : false,
        notify : false,
        ui : false
    });
});