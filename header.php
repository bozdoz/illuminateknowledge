<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'twentysixteen' ); ?></a>
	<header id="masthead" class="site-header" role="banner">
			<div class="site-header-main header-image"
				<?php if ( get_header_image() ) : ?>
				style="background-image: url(<?php header_image(); ?>);"
				<?php endif; // End header image check. ?>
				>
				<div class="site-branding">
					<?php twentysixteen_the_custom_logo(); ?>
				</div><!-- .site-branding -->

				<div class="conditionally-fixed">

				<div id="main-menu">
					<button id="menu-toggle" class="menu-toggle"><?php _e( 'Menu', 'twentysixteen' ); ?></button>

					<div id="site-header-menu" class="site-header-menu">
						<nav id="site-navigation" class="main-navigation" role="navigation" aria-label="Primary Menu">
							<div class="menu-main-container">
								<ul id="menu-main" class="primary-menu">
									<?php

									$the_query = new WP_Query( array(
										'post_type' => 'section',
										'order' => 'ASC',
										'orderby' => 'meta_value_num',
										'meta_key' => 'section_order'
										) );

									// Start the loop.
									while ( $the_query->have_posts() ) : $the_query->the_post();

									$the_title = get_the_title();

									?><li class="menu-item menu-item-type-post_type menu-item-object-page">
										<a href="#<?php echo sanitize_title_with_dashes($the_title); ?>"><?php echo $the_title; ?></a>
									</li><?php
									endwhile;

									/* Restore original Post Data */
									wp_reset_postdata();

									?>
								</ul>
							</div>
						</nav>

						<?php if ( has_nav_menu( 'social' ) ) : ?>
							<nav id="social-navigation" class="social-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Social Links Menu', 'twentysixteen' ); ?>">
								<?php
									wp_nav_menu( array(
										'theme_location' => 'social',
										'menu_class'     => 'social-links-menu',
										'depth'          => 1,
										'link_before'    => '<span class="screen-reader-text">',
										'link_after'     => '</span>',
									) );
								?>
							</nav><!-- .social-navigation -->
						<?php endif; ?>
					</div><!-- .site-header-menu -->
				</div>
				</div>
			</div><!-- .site-header-main .header-image -->
	</header><!-- .site-header -->

	<div class="site-inner">
		<div id="content" class="site-content">
