(function ($) {
	$(window).on('scroll', function () {
		var menuheight = $('#main-menu').height(),
			height = $('#masthead').height() - menuheight,
			scrolltop = $(this).scrollTop();

		if (scrolltop > height) {
			$('.conditionally-fixed').addClass('fix-it').height( menuheight );
		} else {
			$('.conditionally-fixed').removeClass('fix-it');
		}
	});
})(jQuery);