(function ($) {
	wp.customize('menu_background_color', function (value) {
		value.bind( function ( to ) {
			$('#main-menu, .menu-main-container').css('background', to);
		});
	});

	wp.customize('footer_background_color', function (value) {
		value.bind( function ( to ) {
			$('.site-footer').css('background', to);
		});
	});

	wp.customize('header_color', function (value) {
		value.bind( function ( to ) {
			$('h2, h3').css('color', to);
		});
	});

	wp.customize('footer_text', function (value) {
		value.bind( function ( to ) {
			$('#footer-text').html(to);
		});
	});

	wp.customize('header_bg_color', function (value) {
		value.bind( function ( to ) {
			$('.header-image').css('background-color', to);
		});
	});
})(jQuery);